package com.example.listviewk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listView = findViewById<ListView>(R.id.listView)

        val postsList = ArrayList<String>()
        postsList.add("Красный")
        postsList.add("Оранжевый")
        postsList.add("Желтый")
        postsList.add("Зелёный")
        postsList.add("Голубой")
        postsList.add("Синий")
        postsList.add("Фиолетовый")
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, postsList)
        listView.adapter = arrayAdapter

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                Toast.makeText(
                    this@MainActivity,
                    "Номер " + position + " - " + postsList[position],
                    Toast.LENGTH_SHORT
                ).show()
            }
    }
}

